# 用 Python 學數學

## 參考文獻

* [用Python學數學(PDF)](http://www.goodbooks.com.tw/python/CH07.pdf)
* [數學與Python有機結合及統計學、微積分、線性代數相關資源、圖形軟體](https://www.jishuwen.com/d/27NM/zh-tw)
* [](https://docs.sympy.org/latest/modules/plotting.html)
* http://www.goodmath.org/blog/
