def distance(p1, p2):
    x1,y1 = p1
    x2,y2 = p2
    dx = x2-x1
    dy = y2-y1
    return dx*dx+dy*dy

p1 = (3,4)
p2 = (0,0)
print('distance({},{})={}'.format(p1, p2, distance(p1, p2)))
