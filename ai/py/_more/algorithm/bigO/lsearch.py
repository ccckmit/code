def find(a, o):
    L = len(a)
    for i in range(L):
        if a[i] == o: return i
    return -1

print('find([a, d, x, b, g], x)=', find(['a','d','x','b','g'], 'x'))
