# IRIS dataset

Run the `main.dart` file to run the example and train a model on the famous `iris` dataset.

```
mac020:iris mac020$ dart main.dart
120
120
30
30
Test: In: [5.1, 3.5, 1.4, 0.2] Pred: [1.0, 0.0, 0.0] Actual: [1.0, 0.0, 0.0] Result: true
Test: In: [4.9, 3.0, 1.4, 0.2] Pred: [1.0, 0.0, 0.0] Actual: [1.0, 0.0, 0.0] Result: true
Test: In: [4.7, 3.2, 1.3, 0.2] Pred: [1.0, 0.0, 0.0] Actual: [1.0, 0.0, 0.0] Result: true
Test: In: [5.4, 3.9, 1.7, 0.4] Pred: [1.0, 0.0, 0.0] Actual: [1.0, 0.0, 0.0] Result: true
Test: In: [4.8, 3.4, 1.6, 0.2] Pred: [1.0, 0.0, 0.0] Actual: [1.0, 0.0, 0.0] Result: true
Test: In: [4.8, 3.0, 1.4, 0.1] Pred: [1.0, 0.0, 0.0] Actual: [1.0, 0.0, 0.0] Result: true
Test: In: [5.7, 4.4, 1.5, 0.4] Pred: [1.0, 0.0, 0.0] Actual: [1.0, 0.0, 0.0] Result: true
Test: In: [4.7, 3.2, 1.6, 0.2] Pred: [1.0, 0.0, 0.0] Actual: [1.0, 0.0, 0.0] Result: true
Test: In: [5.0, 3.2, 1.2, 0.2] Pred: [1.0, 0.0, 0.0] Actual: [1.0, 0.0, 0.0] Result: true
Test: In: [5.5, 3.5, 1.3, 0.2] Pred: [1.0, 0.0, 0.0] Actual: [1.0, 0.0, 0.0] Result: true
Test: In: [4.9, 3.1, 1.5, 0.1] Pred: [1.0, 0.0, 0.0] Actual: [1.0, 0.0, 0.0] Result: true
Test: In: [4.6, 3.2, 1.4, 0.2] Pred: [1.0, 0.0, 0.0] Actual: [1.0, 0.0, 0.0] Result: true
Test: In: [7.0, 3.2, 4.7, 1.4] Pred: [0.0, 1.0, 0.0] Actual: [0.0, 1.0, 0.0] Result: true
Test: In: [6.6, 2.9, 4.6, 1.3] Pred: [0.0, 1.0, 0.0] Actual: [0.0, 1.0, 0.0] Result: true
Test: In: [6.2, 2.2, 4.5, 1.5] Pred: [0.0, 0.0, 1.0] Actual: [0.0, 1.0, 0.0] Result: false
Test: In: [6.0, 2.9, 4.5, 1.5] Pred: [0.0, 1.0, 0.0] Actual: [0.0, 1.0, 0.0] Result: true
Test: In: [5.5, 2.4, 3.8, 1.1] Pred: [0.0, 1.0, 0.0] Actual: [0.0, 1.0, 0.0] Result: true
Test: In: [5.8, 2.7, 3.9, 1.2] Pred: [0.0, 1.0, 0.0] Actual: [0.0, 1.0, 0.0] Result: true
Test: In: [5.6, 2.7, 4.2, 1.3] Pred: [0.0, 1.0, 0.0] Actual: [0.0, 1.0, 0.0] Result: true
Test: In: [5.7, 2.9, 4.2, 1.3] Pred: [0.0, 1.0, 0.0] Actual: [0.0, 1.0, 0.0] Result: true
Test: In: [6.8, 3.0, 5.5, 2.1] Pred: [0.0, 0.0, 1.0] Actual: [0.0, 0.0, 1.0] Result: true
Test: In: [7.7, 3.8, 6.7, 2.2] Pred: [0.0, 0.0, 1.0] Actual: [0.0, 0.0, 1.0] Result: true
Test: In: [7.7, 2.8, 6.7, 2.0] Pred: [0.0, 0.0, 1.0] Actual: [0.0, 0.0, 1.0] Result: true
Test: In: [6.1, 3.0, 4.9, 1.8] Pred: [0.0, 1.0, 1.0] Actual: [0.0, 0.0, 1.0] Result: false
Test: In: [7.2, 3.0, 5.8, 1.6] Pred: [0.0, 0.0, 1.0] Actual: [0.0, 0.0, 1.0] Result: true
Test: In: [6.4, 2.8, 5.6, 2.2] Pred: [0.0, 0.0, 1.0] Actual: [0.0, 0.0, 1.0] Result: true
Test: In: [7.7, 3.0, 6.1, 2.3] Pred: [0.0, 0.0, 1.0] Actual: [0.0, 0.0, 1.0] Result: true
Test: In: [6.0, 3.0, 4.8, 1.8] Pred: [0.0, 1.0, 0.0] Actual: [0.0, 0.0, 1.0] Result: false
Test: In: [6.7, 3.3, 5.7, 2.5] Pred: [0.0, 0.0, 1.0] Actual: [0.0, 0.0, 1.0] Result: true
Test: In: [6.5, 3.0, 5.2, 2.0] Pred: [0.0, 0.0, 1.0] Actual: [0.0, 0.0, 1.0] Result: true
Accuracy: 90.0%, Wrongs: 3, Correct: 27, All: 30
```