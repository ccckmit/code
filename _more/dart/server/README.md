# Dart Server

## 總覽

* https://medium.com/flutter-community/web-server-frameworks-for-dart-197a073299eb
    * Aqueduct -- https://aqueduct.io/
    * Angel -- https://angel-dart.dev/
    * 原生的 -- https://dart.dev/tutorials/server/httpserver, https://dart-lang.github.io/server/, https://pub.dev/packages/grpc
    * shelf -- https://pub.dev/packages/shelf
