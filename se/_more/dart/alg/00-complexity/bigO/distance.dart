import "dart:math";

distance(p1, p2) {
  var dx = p1['x'] - p2['x'];
  var dy = p1['y'] - p2['y'];
  return sqrt(dx*dx+dy*dy);
}

main() {
  var p1 = {'x':3, 'y':4}, p2 = {'x': 0, 'y':0};
  print('distance($p1,$p2)=${distance(p1,p2)}');
}
