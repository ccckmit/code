find(a,o) {
  var len = a.length;
  for (var i=0; i<len; i++) {
    if (a[i] == o) return i;
  }
  return -1;
}

main() {
  var r = find(['a','d','x','b','g'], 'x');
  print('find([a, d, x, b, g], x)=$r');
}

