matrixMul(a, b) {
  var m = a.length, n = a[0].length, p = b[0].length;
  var r = List.generate(m, (i) => List.generate(p, (j) => 0));
  for (var i=0; i<m; i++) {
    for (var j=0; j<n; j++) {
      for (var k=0; k<p; k++) {
        r[i][k] += a[i][j] * b[j][k];
      }
    }
  }
  return r;
}

main() {
  var a = [[1,2,3],[3,2,1]], b=[[1,1],[1,1],[1,1]];
  print(matrixMul(a,b));
}

