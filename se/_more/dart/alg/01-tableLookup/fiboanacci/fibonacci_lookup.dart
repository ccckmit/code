List<int>  fib = [0, 1];

fibonacci(n) {
  if (n < fib.length) return fib[n];
  var r = fibonacci(n - 1) + fibonacci(n - 2);
  fib.add(r);
  return fib[n];
}

main() {
  var startTime = new DateTime.now();
  var n = 40;
  print('fibonacci(${n})=${fibonacci(n)}');
  var endTime = new DateTime.now();
  var difference = endTime.difference(startTime);
  print('time:$difference');
}
