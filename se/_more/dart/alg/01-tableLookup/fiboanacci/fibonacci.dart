fibonacci(n) {
  if (n == 0) return 0;
  if (n == 1) return 1;
  return fibonacci(n - 1) + fibonacci(n - 2);
}

main() {
  var startTime = new DateTime.now();
  var n = 40;
  print('fibonacci(${n})=${fibonacci(n)}');
  var endTime = new DateTime.now();
  var difference = endTime.difference(startTime);
  print('time:$difference');
}
