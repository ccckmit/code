# C(n,k)

```
user@DESKTOP-96FRN6B MINGW64 /d/ccc/ccc109a/se/_more/deno/alg/01-tableLookup/combinatorial (master)
$ deno run Cnk.js
c(5,2)= 10
c(7,3)= 35
c(12,5)= 792
c(60,30)= 118264581564861470

user@DESKTOP-96FRN6B MINGW64 /d/ccc/ccc109a/se/_more/deno/alg/01-tableLookup/combinatorial (master)
$ deno run CnkR.js
c(5,2)= 10
c(7,3)= 35
c(12,5)= 792

user@DESKTOP-96FRN6B MINGW64 /d/ccc/ccc109a/se/_more/deno/alg/01-tableLookup/combinatorial (master)
$ deno run CnkRLookup.js
c(5,2)= 10
C=%j [
 <1 empty item>,
 [ 1, 1 ],
 [ 1, 2, 1 ],
 [ 1, 3, 3 ],
 [ <1 empty item>, 4, 6 ],
 [ <2 empty items>, 10 ]
]
c(7,3)= 35
c(12,5)= 792
c(60,30)= 118264581564861420
```
