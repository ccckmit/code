import 'dart:math';

class Point {
  final num x;
  final num y;
  final num distanceFromOrigin;

  Point(x, y)
      : x = x,
        y = y,
        distanceFromOrigin = sqrt(x * x + y * y);

  // Delegates to the main constructor.
  Point.alongXAxis(double x) : this(x, 0);
}

main() {
  var p = new Point(2, 3);
  print(p.distanceFromOrigin);
  var p2 = Point.alongXAxis(2);
  print('p2=${p2} distanceFromOrigin=${p2.distanceFromOrigin}');
}
