class A {
  dynamic noSuchMethod(Invocation i) => null;
  void foo();
}

main() {
  A a = new A();
  dynamic f = a.foo;
  // Invokes `Object.noSuchMethod`, not `A.noSuchMethod`, so it throws.
  f(42);
}