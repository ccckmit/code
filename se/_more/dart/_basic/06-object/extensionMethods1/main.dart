import 'string_apis.dart';

main() {
  print("'42'.padLeft(5)=${'42'.padLeft(5)}"); // Use a String method.
  print("'42'.parseInt()=${'42'.parseInt()}"); // Use an extension method.
}
