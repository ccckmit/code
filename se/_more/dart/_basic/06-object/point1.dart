import 'dart:math';

class Point {
  double x, y; // Declare x,y, initially null
  Point(double x, double y) {
    // There's a better way to do this, stay tuned.
    this.x = x;
    this.y = y;
  }
  double distanceTo(Point b) {
    double dx = x-b.x; // double dx = this.x-b.x;  (this 可不寫，預設綁定！)
    double dy = y-b.y; // double dy = this.y-b.y;
    return pow(pow(dx,2)+pow(dy,2), 0.5);
  }
}

main() {
  var p = Point(2, 2);

  // Set the value of the instance variable y.
  p.y = 3;

  // Get the value of y.
  assert(p.y == 3);

  // Invoke distanceTo() on p.
  double distance = p.distanceTo(Point(4, 4));
  print('distance=$distance');
}
