class WannabeFunction {
  String call(String a, String b, String c) => '$a $b $c!';
}

var wf = WannabeFunction();
var out = wf('Hi', 'there,', 'gang');

main() => print(out);

/*
PS D:\ccc\ccc109a\se\dart\_basic\08-lib> dart .\callableClass1.dart
Hi there, gang!
*/