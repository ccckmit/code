main() {
  var a,b;
  // Assign value to a
  a = 2;
  // Assign value to b if b is null; otherwise, b stays the same
  b ??= 2;

  a *= 3; // Assign and multiply: a = a * 3
  assert(a == 6);

}