void printElement(int element) {
  print(element);
}

main() {
  var list = [1, 2, 3];

  // Pass printElement as a parameter.
  list.forEach(printElement);

  var list2 = ['apples', 'bananas', 'oranges'];
  list2.forEach((item) {
    print('${list2.indexOf(item)}: $item');
  });
}
