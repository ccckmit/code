/// Sets the [bold] and [hidden] flags ...
var _bold, _hidden;

void enableFlags1(bool bold, bool hidden) {
  _bold = bold;
  _hidden = hidden;
}

void enableFlags2( { bool bold, bool hidden=false } ) {
  _bold = bold;
  _hidden = hidden;
}

main() {
  enableFlags1(true, false);
  enableFlags2(bold: true);
  print('_bold=$_bold _hidden=$_hidden');
}
