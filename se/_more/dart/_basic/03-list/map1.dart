main() {
  var gifts = {
    // Key:    Value
    'first': 'partridge',
    'second': 'turtledoves',
    'fifth': 'golden rings'
  };

  var nobleGases = {
    2: 'helium',
    10: 'neon',
    18: 'argon',
  };

  nobleGases[3] = 'ccc';

  // Add a new key-value pair to an existing map just as you would in JavaScript:
  gifts['fourth'] = 'calling birds'; // Add a key-value pair
  assert(gifts['first'] == 'partridge');
  assert(gifts['fifth'] == null);
  print('gifts.length=${gifts.length}');
}