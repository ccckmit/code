# aqueduct

* https://aqueduct.io/
    * dart 的 web 框架

```
λ pub global activate aqueduct
λ aqueduct create my_app
λ cd my_app
λ aqueduct serve
```

## ccc 操作

Windows 中一開始不能用，但按下列這篇設定環境變數 PUB_CACHE 和 PUB_HOSTED_URL 之後，再重新執行 pub global activate aqueduct 就可以用了。

* https://github.com/stablekernel/aqueduct/issues/837

啟動完畢後打 http://127.0.0.1:8888/example 就可以看到

```
PS D:\ccc\ccc109a\se\dart\_more\server\aqueduct\my_app> aqueduct serve
-- Aqueduct CLI Version: 3.3.0+1
-- Aqueduct project version: 3.3.0+1
-- Preparing...
-- Starting application 'my_app/my_app'
    Channel: MyAppChannel
    Config: D:\ccc\ccc109a\se\dart\_more\server\aqueduct\my_app\config.yaml
    Port: 8888
[INFO] aqueduct: Server aqueduct/1 started.  
[INFO] aqueduct: Server aqueduct/2 started.  
[INFO] aqueduct: GET / 56ms 404   
[INFO] aqueduct: GET /favicon.ico 0ms 404   
[INFO] aqueduct: GET /index.html 51ms 404   
[INFO] aqueduct: GET /example 11ms 200   

```

## log

```
PS D:\ccc\ccc109a\se\dart\_more\server\aqueduct> pub global activate aqueduct
Resolving dependencies...
+ analyzer 0.35.4 (0.39.13 available)
+ aqueduct 3.3.0+1 (4.0.0-b1 available)
+ args 1.6.0
+ async 2.4.2
+ charcode 1.1.3
+ codable 1.0.0
+ collection 1.14.13 (1.15.0-nullsafety available)
+ convert 2.1.1
+ crypto 2.1.5
+ front_end 0.1.14 (0.1.29 available)
+ glob 1.2.0
+ isolate_executor 2.0.2+3
+ js 0.6.2
+ kernel 0.3.14 (0.3.29 available)
+ logging 0.11.4
+ meta 1.2.2 (1.3.0-nullsafety available)
+ node_interop 1.1.1
+ node_io 1.1.1
+ open_api 2.0.1
+ package_config 1.9.3
+ password_hash 2.0.0
+ path 1.7.0
+ pedantic 1.9.2
+ postgres 1.0.2 (2.1.1 available)
+ pub_cache 0.2.3
+ pub_semver 1.4.4
+ safe_config 2.0.2 (3.0.0-b2 available)
+ source_span 1.7.0
+ string_scanner 1.0.5
+ term_glyph 1.1.0
+ typed_data 1.2.0 (1.3.0-nullsafety available)
+ watcher 0.9.7+15
+ yaml 2.2.1
Downloading aqueduct 3.3.0+1...
Downloading password_hash 2.0.0...
Downloading open_api 2.0.1...
Downloading codable 1.0.0...
Downloading safe_config 2.0.2...
Downloading postgres 1.0.2...
Downloading yaml 2.2.1...
Downloading pub_cache 0.2.3...
Downloading logging 0.11.4...
Downloading charcode 1.1.3...
Downloading pub_semver 1.4.4...
Downloading path 1.7.0...
Downloading isolate_executor 2.0.2+3...
Downloading args 1.6.0...
Downloading meta 1.2.2...
Downloading crypto 2.1.5...
Downloading string_scanner 1.0.5...
Downloading convert 2.1.1...
Downloading source_span 1.7.0...
Downloading term_glyph 1.1.0...
Downloading typed_data 1.2.0...
Downloading collection 1.14.13...
Downloading analyzer 0.35.4...
Downloading kernel 0.3.14...
Downloading front_end 0.1.14...
Downloading glob 1.2.0...
Downloading node_io 1.1.1...
Downloading node_interop 1.1.1...
Downloading js 0.6.2...
Downloading pedantic 1.9.2...
Downloading watcher 0.9.7+15...
Downloading async 2.4.2...
Precompiling executables...
Precompiled aqueduct:aqueduct.
Installed executable aqueduct.
Activated aqueduct 3.3.0+1.
PS D:\ccc\ccc109a\se\dart\_more\server\aqueduct> aqueduct create my_app      
-- Aqueduct CLI Version: 3.3.0+1
    Template source is: C:\Users\user\AppData\Local\Pub\Cache\hosted\pub.dartlang.org\pub_cache-0.2.3\hosted\pub.flutter-io.cn\aqueduct-3.3.0+1\templates\default\
    See more templates with 'aqueduct create list-templates'
-- Copying template files to new project directory (D:\ccc\ccc109a\se\dart\_more\server\aqueduct\my_app)...
    Copying contents of C:\Users\user\AppData\Local\Pub\Cache\hosted\pub.dartlang.org\pub_cache-0.2.3\hosted\pub.flutter-io.cn\aqueduct-3.3.0+1\templates\default\.gitignore
    Copying contents of C:\Users\user\AppData\Local\Pub\Cache\hosted\pub.dartlang.org\pub_cache-0.2.3\hosted\pub.flutter-io.cn\aqueduct-3.3.0+1\templates\default\.travis.yml
    Copying contents of C:\Users\user\AppData\Local\Pub\Cache\hosted\pub.dartlang.org\pub_cache-0.2.3\hosted\pub.flutter-io.cn\aqueduct-3.3.0+1\templates\default\analysis_options.yaml
    Copying contents of C:\Users\user\AppData\Local\Pub\Cache\hosted\pub.dartlang.org\pub_cache-0.2.3\hosted\pub.flutter-io.cn\aqueduct-3.3.0+1\templates\default\bin
    Copying contents of C:\Users\user\AppData\Local\Pub\Cache\hosted\pub.dartlang.org\pub_cache-0.2.3\hosted\pub.flutter-io.cn\aqueduct-3.3.0+1\templates\default\config.src.yaml
    Copying contents of C:\Users\user\AppData\Local\Pub\Cache\hosted\pub.dartlang.org\pub_cache-0.2.3\hosted\pub.flutter-io.cn\aqueduct-3.3.0+1\templates\default\lib
    Copying contents of C:\Users\user\AppData\Local\Pub\Cache\hosted\pub.dartlang.org\pub_cache-0.2.3\hosted\pub.flutter-io.cn\aqueduct-3.3.0+1\templates\default\pubspec.yaml
    Copying contents of C:\Users\user\AppData\Local\Pub\Cache\hosted\pub.dartlang.org\pub_cache-0.2.3\hosted\pub.flutter-io.cn\aqueduct-3.3.0+1\templates\default\README.md
    Copying contents of C:\Users\user\AppData\Local\Pub\Cache\hosted\pub.dartlang.org\pub_cache-0.2.3\hosted\pub.flutter-io.cn\aqueduct-3.3.0+1\templates\default\test
    Generating config.yaml from config.src.yaml.
-- Fetching project dependencies (pub get --no-packages-dir )...
-- Please wait...
The --packages-dir flag is no longer used and does nothing.
Resolving dependencies...
+ analyzer 0.35.4 (0.39.13 available)
+ aqueduct 3.3.0+1 (4.0.0-b1 available)
+ aqueduct_test 1.0.1 (2.0.0-b1 available)
+ args 1.6.0
+ async 2.4.2
+ boolean_selector 1.0.5 (2.0.0 available)
+ charcode 1.1.3
+ codable 1.0.0
+ collection 1.14.13 (1.15.0-nullsafety available)
+ convert 2.1.1
+ crypto 2.1.5
+ front_end 0.1.14 (0.1.29 available)
+ glob 1.2.0
+ http 0.12.2
+ http_multi_server 2.2.0
+ http_parser 3.1.4
+ io 0.3.4
+ isolate_executor 2.0.2+3
+ js 0.6.2
+ json_rpc_2 2.2.1
+ kernel 0.3.14 (0.3.29 available)
+ logging 0.11.4
+ matcher 0.12.5 (0.12.9 available)
+ meta 1.2.2 (1.3.0-nullsafety available)
+ mime 0.9.6+3
+ multi_server_socket 1.0.2
+ node_interop 1.1.1
+ node_io 1.1.1
+ node_preamble 1.4.12
+ open_api 2.0.1
+ package_config 1.9.3
+ package_resolver 1.0.10
+ password_hash 2.0.0
+ path 1.7.0
+ pedantic 1.9.2
+ pool 1.4.0
+ postgres 1.0.2 (2.1.1 available)
+ pub_cache 0.2.3
+ pub_semver 1.4.4
+ safe_config 2.0.2 (3.0.0-b2 available)
+ shelf 0.7.7
+ shelf_packages_handler 1.0.4 (2.0.0 available)
+ shelf_static 0.2.8
+ shelf_web_socket 0.2.3
+ source_map_stack_trace 1.1.5 (2.0.0 available)
+ source_maps 0.10.9
+ source_span 1.7.0
+ stack_trace 1.9.5
+ stream_channel 2.0.0
+ string_scanner 1.0.5
+ term_glyph 1.1.0
+ test 1.6.3 (1.15.3 available)
+ test_api 0.2.5 (0.2.18 available)
+ test_core 0.2.5 (0.3.11 available)
+ typed_data 1.2.0 (1.3.0-nullsafety available)
+ vm_service_client 0.2.6+3
+ watcher 0.9.7+15
+ web_socket_channel 1.1.0
+ yaml 2.2.1
Downloading aqueduct_test 1.0.1...
Downloading matcher 0.12.5...
Downloading multi_server_socket 1.0.2...
Downloading test 1.6.3...
Downloading test_core 0.2.5...
Downloading test_api 0.2.5...
Downloading stream_channel 2.0.0...
Downloading source_map_stack_trace 1.1.5...
Downloading shelf_static 0.2.8...
Downloading stack_trace 1.9.5...
Downloading shelf_packages_handler 1.0.4...
Downloading io 0.3.4...
Downloading boolean_selector 1.0.5...
Downloading pool 1.4.0...
Downloading source_maps 0.10.9...
Downloading shelf_web_socket 0.2.3...
Downloading http_multi_server 2.2.0...
Downloading vm_service_client 0.2.6+3...
Downloading package_resolver 1.0.10...
Downloading shelf 0.7.7...
Downloading node_preamble 1.4.12...
Downloading json_rpc_2 2.2.1...
Downloading web_socket_channel 1.1.0...
Downloading http 0.12.2...
Changed 59 dependencies!
    Success.
-- New project 'my_app' successfully created.
    Project is located at D:\ccc\ccc109a\se\dart\_more\server\aqueduct\my_app
    Open this directory in IntelliJ IDEA, Atom or VS Code.
    See D:\ccc\ccc109a\se\dart\_more\server\aqueduct\my_app\README.md for more information.
PS D:\ccc\ccc109a\se\dart\_more\server\aqueduct> cd my_app
PS D:\ccc\ccc109a\se\dart\_more\server\aqueduct\my_app> aqueduct serve
-- Aqueduct CLI Version: 3.3.0+1
-- Aqueduct project version: 3.3.0+1
-- Preparing...
-- Starting application 'my_app/my_app'
    Channel: MyAppChannel
    Config: D:\ccc\ccc109a\se\dart\_more\server\aqueduct\my_app\config.yaml
    Port: 8888
[INFO] aqueduct: Server aqueduct/1 started.  
[INFO] aqueduct: Server aqueduct/2 started.  

```