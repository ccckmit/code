# dart publish

* https://dart.dev/tools/pub/publishing


注意：在下面打開網址驗證時，用我的 Google Chrome 會失敗，但改用 Microsoft Edge 就成功了！

```
PS D:\ccc\ccc109a\se\dart\se\package\publish1\numda> pub publish --dry-run
Publishing numda 0.0.1 to https://pub.dev/:
|-- .dart_tool
|-- .gitignore
|-- .packages
|-- CHANGELOG.md
|-- LICENSE
|-- README.md
|-- example
|   |-- complex1.dart
|   '-- test1.dart
|-- lib
|   |-- complex.dart
|   '-- numda.dart
|-- pubspec.yaml
'-- test
    '-- test1.dart

Package has 0 warnings.
PS D:\ccc\ccc109a\se\dart\se\package\publish1\numda> pub publish
Publishing numda 0.0.1 to https://pub.dev/:
|-- .dart_tool
|   '-- package_config.json
|-- .gitignore
|-- .packages
|-- CHANGELOG.md
|-- LICENSE
|-- README.md
|-- example
|   |-- complex1.dart
|   '-- test1.dart
|-- lib
|   |-- complex.dart
|   '-- numda.dart
|-- pubspec.yaml
'-- test
    '-- test1.dart

Uploads to pub.dev are subject to https://pub.dev/policy

Do you want to publish numda 0.0.1 (y/N)? y
Pub needs your authorization to upload packages on your behalf.
In a web browser, go to https://accounts.google.com/o/oauth2/auth?access_type=offline&approval_prompt=force&response_type=code&client_id=818368855108-8grd2eg9tj9f38os6f1urbcvsq399u8n.apps.googleusercontent.com&redirect_uri=http%3A%2F%2Flocalhost%3A50477&scope=openid+https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fuserinfo.email       
Then click "Allow access".

Waiting for your authorization...
Authorization received, processing...
Successfully authorized.
Uploading...
Successfully uploaded package.
PS D:\ccc\ccc109a\se\dart\se\package\publish1\numda> 
```

## 第二次

```
PS D:\ccc\ccc109a\se\dart\se\package\publish1\numda> pub publish
Publishing numda 0.0.2 to https://pub.dev/:
|-- .gitignore
|-- CHANGELOG.md
|-- LICENSE
|-- example
|   '-- complex1.dart
|-- lib
|   |-- complex.dart
|   '-- numda.dart
|-- pubspec.yaml
'-- test
    '-- test1.dart
Package validation found the following potential issue:
* .\CHANGELOG.md doesn't mention current version (0.0.2).
  Consider updating it with notes on this version prior to publication.

Uploads to pub.dev are subject to https://pub.dev/policy

Package has 1 warning. Do you want to publish numda 0.0.2 (y/N)? N
Package upload canceled.
PS D:\ccc\ccc109a\se\dart\se\package\publish1\numda> pub publish
Publishing numda 0.0.2 to https://pub.dev/:
|-- .gitignore
|-- CHANGELOG.md
|-- LICENSE
|-- README.md
|-- example
|   '-- complex1.dart
|-- lib
|   |-- complex.dart
|   '-- numda.dart
|-- pubspec.yaml
'-- test
    '-- test1.dart

Uploads to pub.dev are subject to https://pub.dev/policy

Do you want to publish numda 0.0.2 (y/N)? y
Uploading...
Successfully uploaded package.
```
