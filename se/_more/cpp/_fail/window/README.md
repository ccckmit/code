# 微軟的 cl 編譯器

## 
https://code.visualstudio.com/docs/cpp/config-msvc


You can also install just the C++ Build Tools, without a full Visual Studio IDE installation. From the Visual Studio Downloads page, scroll down until you see Tools for Visual Studio under the All downloads section and select the download for Build Tools for Visual Studio.

https://visualstudio.microsoft.com/zh-hant/downloads/

適用於 Visual Studio 2019 的 Microsoft Visual C++ 可轉散發套件



## 環境設定

加入下列兩個路徑到系統路徑裏

* C:\Program Files (x86)\Microsoft Visual Studio\2019\Community\Common7\Tools\vsdevcmd\core

* C:\Program Files (x86)\Microsoft Visual Studio\2019\Community\VC\Tools\MSVC\14.25.28610\bin\Hostx64\x64

## 改用 

D:\ccc\ccc109a\se\c\window>cl hello.c
Microsoft (R) C/C++ Optimizing Compiler Version 19.25.28614 for x64
Copyright (C) Microsoft Corporation.  著作權所有，並保留一切權利。

hello.c
C:\Program Files (x86)\Windows Kits\10\include\10.0.18362.0\ucrt\corecrt.h(10): fatal error C1083: 無法開啟包含檔案: 'vcruntime.h': No such file or directory


D:\ccc\ccc109a\se\c\window>cl hello.c
Microsoft (R) C/C++ Optimizing Compiler Version 19.25.28614 for x64
Copyright (C) Microsoft Corporation.  著作權所有，並保留一切權利。

hello.c
C:\Program Files (x86)\Windows Kits\10\include\10.0.18362.0\ucrt\corecrt.h(10): fatal error C1083: 無法開啟包含檔案: 'vcruntime.h': No such file or directory

## 

Cannot find corecrt.h: $(UniversalCRT_IncludePath) is wrong

For Visual Studio 2017 I had to:

Run Visual Studio Installer.
Select Modify button.
Go to "Individual Components" tab.
Scroll down to "Compilers, build tools and runtimes".
Tick "Windows Universal CRT SDK".
Install.
