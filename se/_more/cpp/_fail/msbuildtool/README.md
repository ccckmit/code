# BuildTool

失敗

```
C:\Windows\System32>vsdevcmd.bat
[DEBUG:VsDevCmd] Writing pre-initialization environment to C:\Users\user\AppData\Local\Temp\dd_vsdevcmd16_preinit_env.log
**********************************************************************
** Visual Studio 2019 Developer Command Prompt v16.6.4
** Copyright (c) 2020 Microsoft Corporation
**********************************************************************
[ERROR:core\msbuild.bat] init:FAILED code:1
[DEBUG:VsDevCmd.bat] Sending telemetry
[ERROR:VsDevCmd.bat] *** VsDevCmd.bat encountered errors. Environment may be incomplete and/or incorrect. ***
[ERROR:VsDevCmd.bat] In an uninitialized command prompt, please 'set VSCMD_DEBUG=[value]' and then re-run
[ERROR:VsDevCmd.bat] vsdevcmd.bat [args] for additional details.
[ERROR:VsDevCmd.bat] Where [value] is:
[ERROR:VsDevCmd.bat]    1 : basic debug logging
[ERROR:VsDevCmd.bat]    2 : detailed debug logging
[ERROR:VsDevCmd.bat]    3 : trace level logging. Redirection of output to a file when using this level is recommended.
[ERROR:VsDevCmd.bat] Example: set VSCMD_DEBUG=3
[ERROR:VsDevCmd.bat]          vsdevcmd.bat > vsdevcmd.trace.txt 2>&1
[DEBUG:VsDevCmd] Writing post-execution environment to C:\Users\user\AppData\Local\Temp\dd_vsdevcmd16_env.log
```

解決方法，嘗試了還是失敗

https://developercommunity.visualstudio.com/idea/816881/vs2019-vsdevcmdbat-error.html

```
avatar image
Charles Willis [MSFT]
2019年11月12日 15:42
Thank you for providing the extra information. As documented here, this is not supported at this time: https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/compiler-options/how-to-set-environment-variables-for-the-visual-studio-command-line

I’ve converted this to a suggestion so that we can gather feedback from other users to help future prioritization of adding this support.

As a workaround, you might consider launching the VS2019 build from a new cmd.exe process. In your master build script,
start /wait /i /b "VS2019" CMD /c "path\to\vsdevcmd && msbuild path\to\sln"

start /wait will block the main script until the new process finishes
/i will start the new process with the default environment (so VSINSTALLDIR won’t be set)
/b will make the new process run in the current window
“VS2019” is a title, but it won’t be displayed anywhere. This can be any string in quotes.

CMD /c will cause CMD to exit after it finishes executing the command

the remaining arguments in quotes are passed to CMD. In the example, it will run vsdevcmd and in the same cmd call msbuild

Thanks again for taking the time to provide feedback.
```

失敗狀況

```
D:\ccc\ccc109a>start /wait /i /b "VS2019" CMD /c "C:\Program Files (x86)\Microsoft Visual Studio\2019\BuildTools\Common7\Tools\vsdevcmd.bat"
**********************************************************************
** Visual Studio 2019 Developer Command Prompt v16.6.4
** Copyright (c) 2020 Microsoft Corporation
**********************************************************************
[ERROR:VsDevCmd.bat] *** VsDevCmd.bat encountered errors. Environment may be incomplete and/or incorrect. ***   
[ERROR:VsDevCmd.bat] In an uninitialized command prompt, please 'set VSCMD_DEBUG=[value]' and then re-run       
[ERROR:VsDevCmd.bat] vsdevcmd.bat [args] for additional details.
[ERROR:VsDevCmd.bat] Where [value] is:
[ERROR:VsDevCmd.bat]    1 : basic debug logging
[ERROR:VsDevCmd.bat]    2 : detailed debug logging
[ERROR:VsDevCmd.bat]    3 : trace level logging. Redirection of output to a file when using this level is recommended.
[ERROR:VsDevCmd.bat] Example: set VSCMD_DEBUG=3
[ERROR:VsDevCmd.bat]          vsdevcmd.bat > vsdevcmd.trace.txt 2>&1
```