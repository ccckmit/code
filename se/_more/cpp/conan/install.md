# conan 套件管理員

```
PS D:\ccc\ccc109a\sp\project\cua> pip install conan
Requirement already satisfied: conan in c:\users\user\appdata\local\programs\python\python37\lib\site-packages (1.25.1)
Requirement already satisfied: PyYAML<6.0,>=3.11 in c:\users\user\appdata\local\programs\python\python37\lib\site-packages (from conan) (5.1.2)
Requirement already satisfied: node-semver==0.6.1 in c:\users\user\appdata\local\programs\python\python37\lib\site-packages (from conan) (0.6.1)
Requirement already satisfied: distro<1.2.0,>=1.0.2 in c:\users\user\appdata\local\programs\python\python37\lib\site-packages (from conan) (1.1.0)
Requirement already satisfied: pygments<3.0,>=2.0 in c:\users\user\appdata\roaming\python\python37\site-packages (from conan) (2.4.2)
Requirement already satisfied: deprecation<2.1,>=2.0 in c:\users\user\appdata\local\programs\python\python37\lib\site-packages (from conan) (2.0.7)
Requirement already satisfied: requests<3.0.0,>=2.8.1 in c:\users\user\appdata\local\programs\python\python37\lib\site-packages (from conan) (2.22.0)
Requirement already satisfied: future<0.19.0,>=0.16.0 in c:\users\user\appdata\local\programs\python\python37\lib\site-packages (from conan) (0.18.2)
Requirement already satisfied: tqdm<5,>=4.28.1 in c:\users\user\appdata\local\programs\python\python37\lib\site-packages (from conan) (4.36.1)
Requirement already satisfied: six<=1.14.0,>=1.10.0 in c:\users\user\appdata\roaming\python\python37\site-packages (from conan) (1.12.0)
Requirement already satisfied: Jinja2<3,>=2.3 in c:\users\user\appdata\roaming\python\python37\site-packages (from conan) (2.10.1)
Requirement already satisfied: python-dateutil<3,>=2.7.0 in c:\users\user\appdata\roaming\python\python37\site-packages (from conan) (2.8.0)
Requirement already satisfied: colorama<0.5.0,>=0.3.3 in c:\users\user\appdata\roaming\python\python37\site-packages (from conan) (0.4.1)
Requirement already satisfied: urllib3!=1.25.4,!=1.25.5 in c:\users\user\appdata\local\programs\python\python37\lib\site-packages (from conan) (1.22)
Requirement already satisfied: patch-ng<1.18,>=1.17.4 in c:\users\user\appdata\local\programs\python\python37\lib\site-packages (from conan) (1.17.4)
Requirement already satisfied: PyJWT<2.0.0,>=1.4.0 in c:\users\user\appdata\local\programs\python\python37\lib\site-packages (from conan) (1.7.1)
Requirement already satisfied: pluginbase<1.0,>=0.5 in c:\users\user\appdata\local\programs\python\python37\lib\site-packages (from conan) (0.7)
Requirement already satisfied: bottle<0.13,>=0.12.8 in c:\users\user\appdata\local\programs\python\python37\lib\site-packages (from conan) (0.12.18)
Requirement already satisfied: fasteners>=0.14.1 in c:\users\user\appdata\local\programs\python\python37\lib\site-packages (from conan) (0.15)
Requirement already satisfied: packaging in c:\users\user\appdata\local\programs\python\python37\lib\site-packages (from deprecation<2.1,>=2.0->conan) (20.3)
Requirement already satisfied: chardet<3.1.0,>=3.0.2 in c:\users\user\appdata\local\programs\python\python37\lib\site-packages (from requests<3.0.0,>=2.8.1->conan) (3.0.4)
Requirement already satisfied: certifi>=2017.4.17 in c:\users\user\appdata\local\programs\python\python37\lib\site-packages (from requests<3.0.0,>=2.8.1->conan) (2019.9.11)
Requirement already satisfied: idna<2.9,>=2.5 in c:\users\user\appdata\local\programs\python\python37\lib\site-packages (from requests<3.0.0,>=2.8.1->conan) (2.6)
Requirement already satisfied: MarkupSafe>=0.23 in c:\users\user\appdata\roaming\python\python37\site-packages (from Jinja2<3,>=2.3->conan) (1.1.1)
Requirement already satisfied: monotonic>=0.1 in c:\users\user\appdata\local\programs\python\python37\lib\site-packages (from fasteners>=0.14.1->conan) (1.5)
Requirement already satisfied: pyparsing>=2.0.2 in c:\users\user\appdata\local\programs\python\python37\lib\site-packages (from packaging->deprecation<2.1,>=2.0->conan) (2.4.2)
WARNING: You are using pip version 19.3.1; however, version 20.1.1 is available.
You should consider upgrading via the 'python -m pip install --upgrade pip' command.
```