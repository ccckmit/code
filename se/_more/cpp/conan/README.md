# conan 套件管理員

```
$ pip install conan

$ conan

```

## Linux

```
root@localhost:/home/guest/code/se/c/conan/hello_md5# pip install conan
Collecting conan
  Downloading https://files.pythonhosted.org/packages/16/37/aa79cced9c221a4d832f3ae0aa5cfe9fb106d71e26b21dc5e53169a18c0f/conan-1.27.1.tar.gz (600kB)
    100% |████████████████████████████████| 604kB 1.4MB/s
Collecting Jinja2<3,>=2.3 (from conan)
  Using cached https://files.pythonhosted.org/packages/30/9e/f663a2aa66a09d838042ae1a2c5659828bb9b41ea3a6efa20a20fd92b121/Jinja2-2.11.2-py2.py3-none-any.whl
Collecting PyJWT<2.0.0,>=1.4.0 (from conan)
  Using cached https://files.pythonhosted.org/packages/87/8b/6a9f14b5f781697e51259d81657e6048fd31a113229cf346880bb7545565/PyJWT-1.7.1-py2.py3-none-any.whl
Collecting PyYAML<6.0,>=3.11 (from conan)
  Downloading https://files.pythonhosted.org/packages/64/c2/b80047c7ac2478f9501676c988a5411ed5572f35d1beff9cae07d321512c/PyYAML-5.3.1.tar.gz (269kB)
    100% |████████████████████████████████| 276kB 2.9MB/s
Collecting bottle<0.13,>=0.12.8 (from conan)
  Downloading https://files.pythonhosted.org/packages/d9/4f/57887a07944140dae0d039d8bc270c249fc7fc4a00744effd73ae2cde0a9/bottle-0.12.18.tar.gz (71kB)
    100% |████████████████████████████████| 71kB 6.8MB/s
Collecting colorama<0.5.0,>=0.3.3 (from conan)
  Downloading https://files.pythonhosted.org/packages/c9/dc/45cdef1b4d119eb96316b3117e6d5708a08029992b2fee2c143c7a0a5cc5/colorama-0.4.3-py2.py3-none-any.whl
Collecting deprecation<2.1,>=2.0 (from conan)
  Using cached https://files.pythonhosted.org/packages/b9/2a/d5084a8781398cea745c01237b95d9762c382697c63760a95cc6a814ad3a/deprecation-2.0.7-py2.py3-none-any.whl
Collecting distro<1.2.0,>=1.0.2 (from conan)
  Using cached https://files.pythonhosted.org/packages/b0/55/29bfd4d4d4149e860ed01aa446108eb17b240997b746c06a2d0c8ce04f69/distro-1.1.0-py2.py3-none-any.whl
Collecting fasteners>=0.14.1 (from conan)
  Using cached https://files.pythonhosted.org/packages/18/bd/55eb2d6397b9c0e263af9d091ebdb756b15756029b3cededf6461481bc63/fasteners-0.15-py2.py3-none-any.whl
Collecting future<0.19.0,>=0.16.0 (from conan)
  Using cached https://files.pythonhosted.org/packages/45/0b/38b06fd9b92dc2b68d58b75f900e97884c45bedd2ff83203d933cf5851c9/future-0.18.2.tar.gz
Collecting node-semver==0.6.1 (from conan)
  Downloading https://files.pythonhosted.org/packages/f1/4e/1d9a619dcfd9f42d0e874a5b47efa0923e84829886e6a47b45328a1f32f1/node-semver-0.6.1.tar.gz
Collecting patch-ng<1.18,>=1.17.4 (from conan)
  Using cached https://files.pythonhosted.org/packages/c1/b2/ad3cd464101435fdf642d20e0e5e782b4edaef1affdf2adfc5c75660225b/patch-ng-1.17.4.tar.gz
Collecting pluginbase<1.0,>=0.5 (from conan)
  Using cached https://files.pythonhosted.org/packages/6e/f4/1db0a26c1c7fad81a1214ad1b02839a7bd98d8ba68f782f6edcc3d343441/pluginbase-0.7.tar.gz
Collecting pygments<3.0,>=2.0 (from conan)
  Downloading https://files.pythonhosted.org/packages/be/39/32da3184734730c0e4d3fa3b2b5872104668ad6dc1b5a73d8e477e5fe967/Pygments-2.5.2-py2.py3-none-any.whl (896kB)
    100% |████████████████████████████████| 901kB 1.0MB/s
Collecting python-dateutil<3,>=2.7.0 (from conan)
  Using cached https://files.pythonhosted.org/packages/d4/70/d60450c3dd48ef87586924207ae8907090de0b306af2bce5d134d78615cb/python_dateutil-2.8.1-py2.py3-none-any.whl
Collecting requests<3.0.0,>=2.8.1 (from conan)
  Downloading https://files.pythonhosted.org/packages/45/1e/0c169c6a5381e241ba7404532c16a21d86ab872c9bed8bdcd4c423954103/requests-2.24.0-py2.py3-none-any.whl (61kB)
    100% |████████████████████████████████| 71kB 7.1MB/s
Requirement already satisfied: six<=1.14.0,>=1.10.0 in /usr/lib/python2.7/dist-packages (from conan)
Collecting tqdm<5,>=4.28.1 (from conan)
  Downloading https://files.pythonhosted.org/packages/af/88/7b0ea5fa8192d1733dea459a9e3059afc87819cb4072c43263f2ec7ab768/tqdm-4.48.0-py2.py3-none-any.whl (67kB)
    100% |████████████████████████████████| 71kB 7.3MB/s
Collecting urllib3!=1.25.4,!=1.25.5 (from conan)
  Downloading https://files.pythonhosted.org/packages/9f/f0/a391d1463ebb1b233795cabfc0ef38d3db4442339de68f847026199e69d7/urllib3-1.25.10-py2.py3-none-any.whl (127kB)
    100% |████████████████████████████████| 133kB 6.0MB/s
Collecting MarkupSafe>=0.23 (from Jinja2<3,>=2.3->conan)
  Downloading https://files.pythonhosted.org/packages/fb/40/f3adb7cf24a8012813c5edb20329eb22d5d8e2a0ecf73d21d6b85865da11/MarkupSafe-1.1.1-cp27-cp27mu-manylinux1_x86_64.whl
Collecting packaging (from deprecation<2.1,>=2.0->conan)
  Downloading https://files.pythonhosted.org/packages/46/19/c5ab91b1b05cfe63cccd5cfc971db9214c6dd6ced54e33c30d5af1d2bc43/packaging-20.4-py2.py3-none-any.whl
Collecting monotonic>=0.1 (from fasteners>=0.14.1->conan)
  Using cached https://files.pythonhosted.org/packages/ac/aa/063eca6a416f397bd99552c534c6d11d57f58f2e94c14780f3bbf818c4cf/monotonic-1.5-py2.py3-none-any.whl
Collecting chardet<4,>=3.0.2 (from requests<3.0.0,>=2.8.1->conan)
  Downloading https://files.pythonhosted.org/packages/bc/a9/01ffebfb562e4274b6487b4bb1ddec7ca55ec7510b22e4c51f14098443b8/chardet-3.0.4-py2.py3-none-any.whl (133kB)
    100% |████████████████████████████████| 143kB 6.1MB/s 
Collecting certifi>=2017.4.17 (from requests<3.0.0,>=2.8.1->conan)
  Downloading https://files.pythonhosted.org/packages/5e/c4/6c4fe722df5343c33226f0b4e0bb042e4dc13483228b4718baf286f86d87/certifi-2020.6.20-py2.py3-none-any.whl (156kB)
    100% |████████████████████████████████| 163kB 5.0MB/s
Requirement already satisfied: idna<3,>=2.5 in /usr/lib/python2.7/dist-packages (from requests<3.0.0,>=2.8.1->conan)
Collecting pyparsing>=2.0.2 (from packaging->deprecation<2.1,>=2.0->conan)
  Using cached https://files.pythonhosted.org/packages/8a/bb/488841f56197b13700afd5658fc279a2025a39e22449b7cf29864669b15d/pyparsing-2.4.7-py2.py3-none-any.whl
Building wheels for collected packages: conan, PyYAML, bottle, future, node-semver, patch-ng, pluginbase
  Running setup.py bdist_wheel for conan ... done
  Stored in directory: /root/.cache/pip/wheels/6b/97/66/5cbbc0cb093200a329accda2bbd85bbf8ed09219b8a6e7eed5
  Running setup.py bdist_wheel for PyYAML ... done
  Stored in directory: /root/.cache/pip/wheels/a7/c1/ea/cf5bd31012e735dc1dfea3131a2d5eae7978b251083d6247bd
  Running setup.py bdist_wheel for bottle ... done
  Stored in directory: /root/.cache/pip/wheels/29/92/e1/63b579cc82e270522b383f5070ca5664c26b188f045b0276af
  Running setup.py bdist_wheel for future ... done
  Stored in directory: /root/.cache/pip/wheels/8b/99/a0/81daf51dcd359a9377b110a8a886b3895921802d2fc1b2397e
  Running setup.py bdist_wheel for node-semver ... done
  Stored in directory: /root/.cache/pip/wheels/4c/0c/cf/a86a2b33055f5dbdb1f9741e254070d6e48f5df4e570bf44cb
  Running setup.py bdist_wheel for patch-ng ... done
  Stored in directory: /root/.cache/pip/wheels/5a/5a/a0/61661120c9eb9f88e8eb179e81cfadfdb780dda4246d7c7550
  Running setup.py bdist_wheel for pluginbase ... done
  Stored in directory: /root/.cache/pip/wheels/ce/78/59/1c57c1f57381a115b31c12c969baa20f62b97f7a64c079cce3    
Successfully built conan PyYAML bottle future node-semver patch-ng pluginbase
Installing collected packages: MarkupSafe, Jinja2, PyJWT, PyYAML, bottle, colorama, pyparsing, packaging, deprecation, distro, monotonic, fasteners, future, node-semver, patch-ng, pluginbase, pygments, python-dateutil, urllib3, chardet, certifi, requests, tqdm, conan
Successfully installed Jinja2-2.11.2 MarkupSafe-1.1.1 PyJWT-1.7.1 PyYAML-5.3.1 bottle-0.12.18 certifi-2020.6.20 chardet-3.0.4 colorama-0.4.3 conan-1.27.1 deprecation-2.0.7 distro-1.1.0 fasteners-0.15 future-0.18.2 monotonic-1.5 node-semver-0.6.1 packaging-20.4 patch-ng-1.17.4 pluginbase-0.7 pygments-2.5.2 pyparsing-2.4.7 python-dateutil-2.8.1 requests-2.24.0 tqdm-4.48.0 urllib3-1.25.10
root@localhost:/home/guest/code/se/c/conan/hello_md5# conan
WARN: Migration: Updating settings.yml
Migration: Settings already up to date
Consumer commands
  install    Installs the requirements specified in a recipe (conanfile.py or conanfile.txt).
  config     Manages Conan configuration.
  get        Gets a file or list a directory of a given reference or package.
  info       Gets information about the dependency graph of a recipe.
  search     Searches package recipes and binaries in the local cache or a remote.
Creator commands
  new        Creates a new package recipe template with a 'conanfile.py' and optionally,
             'test_package' testing files.
  create     Builds a binary package for a recipe (conanfile.py).
  upload     Uploads a recipe and binary packages to a remote.
  export     Copies the recipe (conanfile.py & associated files) to your local cache.
  export-pkg Exports a recipe, then creates a package from local source and build folders.
  test       Tests a package consuming it from a conanfile.py with a test() method.
Package development commands
  source     Calls your local conanfile.py 'source()' method.
  build      Calls your local conanfile.py 'build()' method.
  package    Calls your local conanfile.py 'package()' method.
  editable   Manages editable packages (packages that reside in the user workspace, but are
             consumed as if they were in the cache).
  workspace  Manages a workspace (a set of packages consumed from the user workspace that
             belongs to the same project).
Misc commands
  profile    Lists profiles in the '.conan/profiles' folder, or shows profile details.
  remote     Manages the remote list and the package recipes associated with a remote.
  user       Authenticates against a remote with user/pass, caching the auth token.
  imports    Calls your local conanfile.py or conanfile.txt 'imports' method.
  copy       Copies conan recipes and packages to another user/channel.
  remove     Removes packages or binaries matching pattern from local cache or remote.
  alias      Creates and exports an 'alias package recipe'.
  download   Downloads recipe and binaries to the local cache, without using settings.
  inspect    Displays conanfile attributes, like name, version, and options. Works locally,
             in local cache and remote.
  help       Shows help for a specific command.
  graph      Generates and manipulates lock files.
  frogarian  Conan The Frogarian

Conan commands. Type "conan <command> -h" for help
```

## 詳細動作

```
PS D:\ccc\ccc109a\sp\project\cua> pip install conan
Requirement already satisfied: conan in c:\users\user\appdata\local\programs\python\python37\lib\site-packages (1.25.1)
Requirement already satisfied: PyYAML<6.0,>=3.11 in c:\users\user\appdata\local\programs\python\python37\lib\site-packages (from conan) (5.1.2)
Requirement already satisfied: node-semver==0.6.1 in c:\users\user\appdata\local\programs\python\python37\lib\site-packages (from conan) (0.6.1)
Requirement already satisfied: distro<1.2.0,>=1.0.2 in c:\users\user\appdata\local\programs\python\python37\lib\site-packages (from conan) (1.1.0)
Requirement already satisfied: pygments<3.0,>=2.0 in c:\users\user\appdata\roaming\python\python37\site-packages (from conan) (2.4.2)
Requirement already satisfied: deprecation<2.1,>=2.0 in c:\users\user\appdata\local\programs\python\python37\lib\site-packages (from conan) (2.0.7)
Requirement already satisfied: requests<3.0.0,>=2.8.1 in c:\users\user\appdata\local\programs\python\python37\lib\site-packages (from conan) (2.22.0)
Requirement already satisfied: future<0.19.0,>=0.16.0 in c:\users\user\appdata\local\programs\python\python37\lib\site-packages (from conan) (0.18.2)
Requirement already satisfied: tqdm<5,>=4.28.1 in c:\users\user\appdata\local\programs\python\python37\lib\site-packages (from conan) (4.36.1)
Requirement already satisfied: six<=1.14.0,>=1.10.0 in c:\users\user\appdata\roaming\python\python37\site-packages (from conan) (1.12.0)
Requirement already satisfied: Jinja2<3,>=2.3 in c:\users\user\appdata\roaming\python\python37\site-packages (from conan) (2.10.1)
Requirement already satisfied: python-dateutil<3,>=2.7.0 in c:\users\user\appdata\roaming\python\python37\site-packages (from conan) (2.8.0)
Requirement already satisfied: colorama<0.5.0,>=0.3.3 in c:\users\user\appdata\roaming\python\python37\site-packages (from conan) (0.4.1)
Requirement already satisfied: urllib3!=1.25.4,!=1.25.5 in c:\users\user\appdata\local\programs\python\python37\lib\site-packages (from conan) (1.22)
Requirement already satisfied: patch-ng<1.18,>=1.17.4 in c:\users\user\appdata\local\programs\python\python37\lib\site-packages (from conan) (1.17.4)
Requirement already satisfied: PyJWT<2.0.0,>=1.4.0 in c:\users\user\appdata\local\programs\python\python37\lib\site-packages (from conan) (1.7.1)
Requirement already satisfied: pluginbase<1.0,>=0.5 in c:\users\user\appdata\local\programs\python\python37\lib\site-packages (from conan) (0.7)
Requirement already satisfied: bottle<0.13,>=0.12.8 in c:\users\user\appdata\local\programs\python\python37\lib\site-packages (from conan) (0.12.18)
Requirement already satisfied: fasteners>=0.14.1 in c:\users\user\appdata\local\programs\python\python37\lib\site-packages (from conan) (0.15)
Requirement already satisfied: packaging in c:\users\user\appdata\local\programs\python\python37\lib\site-packages (from deprecation<2.1,>=2.0->conan) (20.3)
Requirement already satisfied: chardet<3.1.0,>=3.0.2 in c:\users\user\appdata\local\programs\python\python37\lib\site-packages (from requests<3.0.0,>=2.8.1->conan) (3.0.4)
Requirement already satisfied: certifi>=2017.4.17 in c:\users\user\appdata\local\programs\python\python37\lib\site-packages (from requests<3.0.0,>=2.8.1->conan) (2019.9.11)
Requirement already satisfied: idna<2.9,>=2.5 in c:\users\user\appdata\local\programs\python\python37\lib\site-packages (from requests<3.0.0,>=2.8.1->conan) (2.6)
Requirement already satisfied: MarkupSafe>=0.23 in c:\users\user\appdata\roaming\python\python37\site-packages (from Jinja2<3,>=2.3->conan) (1.1.1)
Requirement already satisfied: monotonic>=0.1 in c:\users\user\appdata\local\programs\python\python37\lib\site-packages (from fasteners>=0.14.1->conan) (1.5)
Requirement already satisfied: pyparsing>=2.0.2 in c:\users\user\appdata\local\programs\python\python37\lib\site-packages (from packaging->deprecation<2.1,>=2.0->conan) (2.4.2)
WARNING: You are using pip version 19.3.1; however, version 20.1.1 is available.
You should consider upgrading via the 'python -m pip install --upgrade pip' command.


```

help

```
PS D:\ccc\ccc109a\sp\project\cua> conan
Consumer commands
  install    Installs the requirements specified in a recipe (conanfile.py or conanfile.txt).
  config     Manages Conan configuration.
  get        Gets a file or list a directory of a given reference or package.
  info       Gets information about the dependency graph of a recipe.
  search     Searches package recipes and binaries in the local cache or a remote.
Creator commands
  new        Creates a new package recipe template with a 'conanfile.py' and optionally,
             'test_package' testing files.
  create     Builds a binary package for a recipe (conanfile.py).
  upload     Uploads a recipe and binary packages to a remote.
  export     Copies the recipe (conanfile.py & associated files) to your local cache.
  export-pkg Exports a recipe, then creates a package from local source and build folders.
  test       Tests a package consuming it from a conanfile.py with a test() method.
Package development commands
  source     Calls your local conanfile.py 'source()' method.
  build      Calls your local conanfile.py 'build()' method.
  package    Calls your local conanfile.py 'package()' method.
  editable   Manages editable packages (packages that reside in the user workspace, but are
             consumed as if they were in the cache).
  workspace  Manages a workspace (a set of packages consumed from the user workspace that
             belongs to the same project).
Misc commands
  profile    Lists profiles in the '.conan/profiles' folder, or shows profile details.
  remote     Manages the remote list and the package recipes associated with a remote.
  user       Authenticates against a remote with user/pass, caching the auth token.
  imports    Calls your local conanfile.py or conanfile.txt 'imports' method.
  copy       Copies conan recipes and packages to another user/channel.
  remove     Removes packages or binaries matching pattern from local cache or remote.
  alias      Creates and exports an 'alias package recipe'.
  download   Downloads recipe and binaries to the local cache, without using settings.
  inspect    Displays conanfile attributes, like name, version, and options. Works locally,
             in local cache and remote.
  help       Shows help for a specific command.
  graph      Generates and manipulates lock files.
  frogarian  Conan The Frogarian

Conan commands. Type "conan <command> -h" for help
```