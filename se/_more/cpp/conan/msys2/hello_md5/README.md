# hello_md5

必須使用 msys 而非 msys MinGW64

但還是失敗

```
user@DESKTOP-96FRN6B MSYS /d/ccc/ccc109a/se/c/conan/msys2/hello_md5/build
$ conan install .. --build=missing --profile=mingw64

Configuration:
[settings]
arch=x86_64
arch_build=x86_64
build_type=Debug
compiler=gcc
compiler.libcxx=libstdc++11
compiler.version=7.3
os=Windows
os_build=Windows
[options]
[build_requires]
[env]

conanfile.txt: Installing package
Requirements
    openssl/1.1.1g from 'conan-center' - Cache
    poco/1.9.4 from 'conan-center' - Cache
Packages
    openssl/1.1.1g:6ff7ec686229fecdb7f4ed24fe722547ebca59b6 - Build
    poco/1.9.4:f6d2a52519e77be5bba77e7bb27585792f711ac4 - Build

Installing (downloading, building) binaries...
openssl/1.1.1g: Copying sources to build folder

openssl/1.1.1g: Building your package in /home/user/.conan/data/openssl/1.1.1g/_/_/build/6ff7ec686229fecdb7f4ed24fe722547ebca59b6
openssl/1.1.1g: Generator txt created conanbuildinfo.txt
openssl/1.1.1g: Calling build()
openssl/1.1.1g: using target: mingw-conan-Debug-Windows-x86_64-gcc-7.3 -> mingw64
openssl/1.1.1g: my %targets = (
    "mingw-conan-Debug-Windows-x86_64-gcc-7.3" => {
        inherit_from => [ "mingw64" ],
        cflags => add("-m64 -g"),
        cxxflags => add("-m64 -g"),

        includes => add(),
        lflags => add("-m64"),





    },
);

openssl/1.1.1g: ['"mingw-conan-Debug-Windows-x86_64-gcc-7.3"', 'no-shared', '--prefix="/home/user/.conan/data/openssl/1.1.1g/_/_/package/6ff7ec686229fecdb7f4ed24fe722547ebca59b6"', '--openssldir="/home/user/.conan/data/openssl/1.1.1g/_/_/package/6ff7ec686229fecdb7f4ed24fe722547ebca59b6/res"', 'no-unit-test', 'threads', 'PERL=perl', 'no-tests', '--debug']
openssl/1.1.1g:
openssl/1.1.1g: ERROR: Package '6ff7ec686229fecdb7f4ed24fe722547ebca59b6' build failed
openssl/1.1.1g: WARN: Build folder /home/user/.conan/data/openssl/1.1.1g/_/_/build/6ff7ec686229fecdb7f4ed24fe722547ebca59b6
ERROR: openssl/1.1.1g: Error in build() method, line 644
        self._make()
while calling '_make', line 580
        self.run('{perl} ./Configure {args}'.format(perl=self._perl, args=args), win_bash=self._win_bash)
        ConanException: Command only for Windows operating system

```

## 

```
khavishbhundoo commented on 14 Apr 2018
I am using clion with MinGW compiler.Conan is detecting that i am using Visual Studio compiler

Auto detecting your dev setup to initialize the default profile (C:\Users\Name\.conan\profiles\default)

Found Visual Studio 14`

Default settings
        os=Windows
        os_build=Windows
        arch=x86_64
        arch_build=x86_64
        compiler=Visual Studio
        compiler.version=14
        build_type=Release

Indeed now cmake detects a wrong compiler.

CMake Error at .conan/conanbuildinfo.cmake:448 (message):
 Incorrect 'Visual Studio'.  Toolset specifies compiler as 'MSVC' but CMake   detected 'GNU'
```

```
lachlankrautz commented on 15 Apr 2018
Create a profile for your MinGW toolchain here ~/.conan/profiles/mingw64, enter your settings eg.

[build_requires]
[settings]
    os=Windows
    os_build=Windows
    arch=x86_64
    arch_build=x86_64
    compiler=gcc
    compiler.version=7.2
    compiler.libcxx=libstdc++11
    build_type=Debug
[options]
[env]
Then use that profile when installing:

conan install .. --build=missing --profile=mingw64
```

## 

使用 msys2 的 msys 版本

```
$ pacman -S python3-pip
正在解決依賴關係...
正在檢查衝突的套件...

套件 (6) python3-appdirs-1.4.3-2  python3-packaging-16.8-3
         python3-pyparsing-2.2.0-2  python3-setuptools-36.4.0-1
         python3-six-1.10.0-5  python3-pip-9.0.1-3

總計下載大小：  1.95 MiB
總計安裝大小：  9.37 MiB

:: 進行安裝嗎？ [Y/n] y
:: 正在擷取套件...
 python3-pyparsing-2...   105.1 KiB   113K/s 00:01 [#####################] 100%
 python3-six-1.10.0-...    31.3 KiB  1361K/s 00:00 [#####################] 100%
 python3-packaging-1...    41.9 KiB  1552K/s 00:00 [#####################] 100%
 python3-appdirs-1.4...    26.5 KiB  2040K/s 00:00 [#####################] 100%
 python3-setuptools-...   412.0 KiB   232K/s 00:02 [#####################] 100%
 python3-pip-9.0.1-3-any 1382.6 KiB   371K/s 00:04 [#####################] 100%
(6/6) 正在檢查鑰匙圈中的鑰匙                       [#####################] 100%
(6/6) 正在檢查套件完整性                           [#####################] 100%
(6/6) 正在載入套件檔案                             [#####################] 100%
(6/6) 正在檢查檔案衝突                             [#####################] 100%
(6/6) 正在檢查可用磁碟空間                         [#####################] 100%
:: 正在處理套件變更...
(1/6) 正在安裝 python3-pyparsing                   [#####################] 100%
(2/6) 正在安裝 python3-six                         [#####################] 100%
(3/6) 正在安裝 python3-packaging                   [#####################] 100%
(4/6) 正在安裝 python3-appdirs                     [#####################] 100%
(5/6) 正在安裝 python3-setuptools                  [#####################] 100%
(6/6) 正在安裝 python3-pip                         [#####################] 100%


```
