# Googletest

* https://github.com/google/googletest
    * https://github.com/google/googletest/blob/master/googletest/docs/primer.md
    * https://github.com/google/googletest/tree/master/googletest/samples

## googletest-sample


來源 -- https://github.com/KanoComputing/googletest-sample

```
user@DESKTOP-96FRN6B MINGW64 /d/ccc/ccc109a/se/c/test (master)
$ ssh guest@misavo.com

guest@localhost:~$ ls
ccc  selfie  sp  sp2  spMore
guest@localhost:~$ mkdir 108test
guest@localhost:~$ cd 108test
guest@localhost:~/108test$ ls
guest@localhost:~/108test$ git clone https://github.com/KanoComputing/googletest-sample.git
Cloning into 'googletest-sample'...
remote: Enumerating objects: 24, done.
remote: Total 24 (delta 0), reused 0 (delta 0), pack-reused 24
Unpacking objects: 100% (24/24), done.
guest@localhost:~/108test$ ls
googletest-sample
guest@localhost:~/108test$ cd googletest-sample
guest@localhost:~/108test/googletest-sample$ ls
CMakeLists.txt  README.md  src  tests
guest@localhost:~/108test/googletest-sample$ cmake .
-- The C compiler identification is GNU 7.5.0
-- The CXX compiler identification is GNU 7.5.0
-- Check for working C compiler: /usr/bin/cc
-- Check for working C compiler: /usr/bin/cc -- works
-- Detecting C compiler ABI info
-- Detecting C compiler ABI info - done
-- Detecting C compile features
-- Detecting C compile features - done
-- Check for working CXX compiler: /usr/bin/c++
-- Check for working CXX compiler: /usr/bin/c++ -- works
-- Detecting CXX compiler ABI info
-- Detecting CXX compiler ABI info - done
-- Detecting CXX compile features
-- Detecting CXX compile features - done
-- Configuring done
-- Generating done
-- Build files have been written to: /home/guest/108test/googletest-sample/tests/vendor/googletest-download   
Scanning dependencies of target googletest
[ 11%] Creating directories for 'googletest'
[ 22%] Performing download step (git clone) for 'googletest'
Cloning into 'googletest-src'...
Already on 'master'
Your branch is up to date with 'origin/master'.
[ 33%] No patch step for 'googletest'
[ 44%] Performing update step for 'googletest'
Current branch master is up to date.
[ 55%] No configure step for 'googletest'
[ 66%] No build step for 'googletest'
[ 77%] No install step for 'googletest'
[ 88%] No test step for 'googletest'
[100%] Completed 'googletest'
[100%] Built target googletest
-- Found PythonInterp: /usr/bin/python (found version "2.7.17")
-- Looking for pthread.h
-- Looking for pthread.h - found
-- Looking for pthread_create
-- Looking for pthread_create - not found
-- Looking for pthread_create in pthreads
-- Looking for pthread_create in pthreads - not found
-- Looking for pthread_create in pthread
-- Looking for pthread_create in pthread - found
-- Found Threads: TRUE  
-- Configuring done
CMake Error in tests/vendor/googletest-src/googlemock/CMakeLists.txt:
  Target "gmock_main" INTERFACE_INCLUDE_DIRECTORIES property contains path:

    "/home/guest/108test/googletest-sample/tests/vendor/googletest-src/googletest/include"

  which is prefixed in the build directory.


CMake Error in tests/vendor/googletest-src/googlemock/CMakeLists.txt:
  Target "gmock_main" INTERFACE_INCLUDE_DIRECTORIES property contains path:

    "/home/guest/108test/googletest-sample/tests/vendor/googletest-src/googlemock/include"

  which is prefixed in the build directory.


-- Generating done
-- Build files have been written to: /home/guest/108test/googletest-sample
guest@localhost:~/108test/googletest-sample$ cmake --version
cmake version 3.10.2

CMake suite maintained and supported by Kitware (kitware.com/cmake).
guest@localhost:~/108test/googletest-sample$ g++ --version
g++ (Ubuntu 7.5.0-3ubuntu1~18.04) 7.5.0
Copyright (C) 2017 Free Software Foundation, Inc.
This is free software; see the source for copying conditions.  There is NO
warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

guest@localhost:~/108test/googletest-sample$ make
Scanning dependencies of target sample
[  7%] Building CXX object CMakeFiles/sample.dir/src/main.cpp.o
[ 15%] Building CXX object CMakeFiles/sample.dir/src/obj.cpp.o
[ 23%] Building CXX object CMakeFiles/sample.dir/src/obj_operations.cpp.o
[ 30%] Linking CXX executable sample
[ 30%] Built target sample
Scanning dependencies of target gtest
[ 38%] Building CXX object tests/vendor/googletest-build/googletest/CMakeFiles/gtest.dir/src/gtest-all.cc.o
[ 46%] Linking CXX static library ../../../../lib/libgtest.a
[ 46%] Built target gtest
Scanning dependencies of target gmock
[ 53%] Building CXX object tests/vendor/googletest-build/googlemock/CMakeFiles/gmock.dir/src/gmock-all.cc.o
[ 61%] Linking CXX static library ../../../../lib/libgmock.a
[ 61%] Built target gmock
Scanning dependencies of target gmock_main
[ 69%] Building CXX object tests/vendor/googletest-build/googlemock/CMakeFiles/gmock_main.dir/src/gmock_main.cc.o
[ 76%] Linking CXX static library ../../../../lib/libgmock_main.a
[ 76%] Built target gmock_main
Scanning dependencies of target tests
[ 84%] Building CXX object tests/CMakeFiles/tests.dir/src/tests.cpp.o
[ 92%] Building CXX object tests/CMakeFiles/tests.dir/src/test_obj.cpp.o
[100%] Linking CXX executable tests
[100%] Built target tests
guest@localhost:~/108test/googletest-sample$ ./tests/test
-bash: ./tests/test: No such file or directory
guest@localhost:~/108test/googletest-sample$ ./tests/tests
[==========] Running 8 tests from 3 test suites.
[----------] Global test environment set-up.
[----------] 1 test from get_number_test
[ RUN      ] get_number_test.returns_correct_value
[       OK ] get_number_test.returns_correct_value (0 ms)
[----------] 1 test from get_number_test (0 ms total)

[----------] 1 test from ObjTest
[ RUN      ] ObjTest.test_obj_operations_with_mocks
[       OK ] ObjTest.test_obj_operations_with_mocks (0 ms)
[----------] 1 test from ObjTest (0 ms total)

[----------] 6 tests from StringsFixtureName/StringsFixture
[ RUN      ] StringsFixtureName/StringsFixture.test_string_valid/0
[       OK ] StringsFixtureName/StringsFixture.test_string_valid/0 (0 ms)
[ RUN      ] StringsFixtureName/StringsFixture.test_string_valid/1
[       OK ] StringsFixtureName/StringsFixture.test_string_valid/1 (0 ms)
[ RUN      ] StringsFixtureName/StringsFixture.test_string_valid/2
[       OK ] StringsFixtureName/StringsFixture.test_string_valid/2 (0 ms)
[ RUN      ] StringsFixtureName/StringsFixture.test_string_valid/3
[       OK ] StringsFixtureName/StringsFixture.test_string_valid/3 (0 ms)
[ RUN      ] StringsFixtureName/StringsFixture.test_string_valid/4
[       OK ] StringsFixtureName/StringsFixture.test_string_valid/4 (0 ms)
[ RUN      ] StringsFixtureName/StringsFixture.test_string_valid/5
[       OK ] StringsFixtureName/StringsFixture.test_string_valid/5 (0 ms)
[----------] 6 tests from StringsFixtureName/StringsFixture (1 ms total)

[----------] Global test environment tear-down
[==========] 8 tests from 3 test suites ran. (2 ms total)
[  PASSED  ] 8 tests.
```