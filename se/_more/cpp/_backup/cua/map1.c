#include <cua.h>

Pair jList[] = {
  {"",   "000"}, {"JGT","001"}, {"JEQ","010"}, {"JGE","011"},
  {"JLT","100"}, {"JNE","101"}, {"JLE","110"}, {"JMP","111"}
};

Map jMap;

int main() {
  map_new(&jMap, 23);
  map_add_list(&jMap, jList, LENGTH(jList));
  map_dump(&jMap);
  char *jle = map_lookup(&jMap, "JLE");
  printf("jle=%s\n", jle);
  char *xxx = map_lookup(&jMap, "XXX");
  printf("xxx=%s\n", xxx);
  map_free(&jMap);
}
