#ifndef __CUA__
#include <string.h>
#include <stdint.h>
#include <stdio.h>
#include <stddef.h>
#include <assert.h>
#include <stdlib.h>

typedef char i8;
typedef unsigned char u8;
typedef short i16;
typedef unsigned short u16;
typedef long i32;
typedef unsigned long u32;
typedef long long i64;
typedef unsigned long long u64;
typedef unsigned int uint;

typedef struct _String {
  char *str;
  int capacity;
} String;

String *str_new(char *str);
void str_extend(String *self, int size);
void str_assign(String *self, char *s);
void str_dump(String *self);

typedef struct _Pair {
  char *key;
  void *value;
} Pair;

typedef struct _Map {
  Pair *table;
  int size;
  int top;
} Map;

typedef struct _StrTable {
  char *text;
  int size;
  char *textEnd;
} StrTable;


extern uint hash(char *key);
extern Map* map_new(Map *map, int size);
extern Pair* map_find(Map *map, char *key);
extern Pair* map_add(Map *map, char *key, void *value);
extern void map_add_list(Map *map, Pair *list, int top);
extern void* map_lookup(Map *map, char *key);
extern void map_dump(Map *map);
extern void map_free(Map *map);
extern void map_init(Map *map, int mSize, Pair *list, int lSize);
extern char *strdup(const char *s);
extern void stb_new(StrTable *t, int size);
extern char *stb_add(StrTable *t, char *str);
extern void stb_free(StrTable *t);
extern void htob(char* hex, char* binary);
extern void itob(int i, char* binary);
extern int btoi(char* binary);

#define SIZE(list) (sizeof(list)/sizeof(list[0]))
#define DEBUG(...) printf(__VA_ARGS__)
#define BIT(i) (0x0001<<(i))

#endif