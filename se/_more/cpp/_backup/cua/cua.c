#include "cua.h"

// ================= Binary ==================
static char* h2b[] = {
  "0000", "0001", "0010", "0011",
  "0100", "0101", "0110", "0111", 
  "1000", "1001", "1010", "1011",
  "1100", "1101", "1110", "1111"};

static char hexDigits[] = "0123456789ABCDEF";

void htob(char* hex, char* binary) {
  for (int i=0; hex[i] != '\0'; i++) {
    char *ptr = strchr(hexDigits, hex[i]);
    assert(ptr != NULL);
    char h = ptr - hexDigits;
    sprintf(&binary[4*i], "%s", h2b[h]);
  }
}

void itob(int i, char* binary) {
  char hex[100];
  sprintf(hex, "%04X", i);
  htob(hex, binary);
}

int btoi(char* binary) {
  int result = 0;
  int len = strlen(binary);
  for(int i=0; i < len; i++) {
    result <<= 1;
    if (binary[i] == '1') result += 1;
  }
  return result;
}

// -------------------- StrTable -----------------------
void stb_new(StrTable *t, int size) {
  t->text = malloc(size);
  t->size = size;
  t->textEnd = t->text;
}

char *stb_add(StrTable *t, char *str) {
  char *strBegin = t->textEnd;
  strcpy(t->textEnd, str);
  t->textEnd += strlen(str) + 1;
  return strBegin;
}

void stb_free(StrTable *t) {
  free(t->text);
}

// -------------------- Str ----------------------------
String *str_new(char *str) {
  String *self = malloc(sizeof(String));
  self->str = strdup(str);
  self->capacity = strlen(str)+1;
  return self;
}

void str_extend(String *self, int size) {
  if (self->capacity > size) return;
  void *new_ptr = realloc(self->str, size);
  if (new_ptr) {
    self->capacity = size;
  } else {
    strcpy(new_ptr, self->str);
    free(self->str);
    self->str = new_ptr;
  }
}

void str_assign(String *self, char *s) {
  str_extend(self, strlen(s)+1);
  strcpy(self->str, s);
}

void str_dump(String *self) {
  printf("address=%p len=%d str=%s\n", self->str, self->capacity, self->str);
}

// -------------------- Map ----------------------------
// 這個 hash 函數比較簡單，但是 R1, R2, R3 這樣的字串會連續，所以不是很好。
// 如果要避免碰撞後搜尋次數太多，可以用更好的 hash 函數
uint hash(char *key) {
  char *p = key;
  uint  h = 37;
  while (*p != '\0') {
    h = (h << 4) + h + *p; // h = h*17 + *p; 為了加速不用乘法 ....
    p++;
  }
  return h;
}

Map* map_new(Map *map, int size) {
  map->table = calloc(size, sizeof(Pair)); // malloc(size*sizeof(Pair));
  map->size = size;
  map->top = 0;
  return map;
}

Pair *map_find(Map *map, char *key) {
  int h = hash(key) % map->size;
  while (map->table[h].key != NULL) {
    if (strcmp(map->table[h].key, key)==0) break;
    h = (h+1) % map->size;
  }
  return &map->table[h];
}

Pair* map_add(Map *map, char *key, void *value) {
  assert(map->top < map->size);
  Pair *p = map_find(map, key);
  if (p->key == NULL) map->top++;
  p->key = key;
  p->value = value;
  return p;
}

void* map_lookup(Map *map, char *key) {
  Pair *p = map_find(map, key);
  return p->value;
}

void map_add_list(Map *map, Pair *list, int top) {
  for (int i=0; i<top; i++) {
    // printf("%d : key=%s value=%s\n", i, list[i].key, list[i].value);
    map_add(map, list[i].key, list[i].value);
  }
}

void map_dump(Map *map) {
  printf("======= mapDump() ==============\n");
  for (int i=0; i<map->size; i++) {
    Pair *p = &map->table[i];
    if (p->key != NULL) {
      printf("%d:  %s %s\n", i, p->key, (char*) p->value);
    }
  }
}

void map_free(Map *map) {
  free(map->table);
}

void map_init(Map *map, int mSize, Pair *list, int lSize) {
  map_new(map, mSize);
  map_add_list(map, list, lSize);
}