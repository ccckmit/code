

# MSYS2

* [Build instructions for Mingw64/Msys2 under Windows 10](https://github.com/stevengj/nlopt/issues/253)

```
cmake -G"MSYS Makefiles"
```

I found the problem. I had the generic cmake installed. I deleted it and installed the mingw version using
pacman -S mingw-w64-x86_64-cmake and now the build works using cmake -G"MSYS Makefiles" ..

Perhaps you can update the instructions to make the version of cmake needed clear. Thanks for your help.

* [glib:windows下基于MSYS2环境编译glib2的过程](https://blog.csdn.net/10km/article/details/80399355)