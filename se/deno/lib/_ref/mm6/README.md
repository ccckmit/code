# mm6 -- Mathematics & Matrix Library

mm6 is a mathematics library for javascript, include package

1. vector
2. matrix
3. tensor
4. statistics
5. probability
6. function
7. calculus
8. geometry
9. optimize
10. polynomial

## Install

```
$ npm install mm6
```

## Example

```js
const uu6 = require('uu6')
const mm6 = require('../src')
const {T} = mm6

let shape = [2,2,3], idx=[1,1,2]
let v = uu6.range(0,12)
console.log('v=%j', v)

let t = T.new({r:v, shape:shape})
console.log('t.get(%j)=', idx, T.get(t, ...idx))
console.log('t=%s', T.str(t))

T.reshape(t, [3,4])

console.log('t=%s', T.str(t))

let nd = T.tensor2ndarray(t)
console.log('nd=%j', nd)

let t2 = T.ndarray2tensor(nd)
console.log('t2=%j', t2)

let t3 = T.add(t, t)
console.log('t3=%j', t3)

let t4 = T.sub(t, t)
console.log('t4=%j', t4)
```

Run

```
$ node tensor1.js
v=[0,1,2,3,4,5,6,7,8,9,10,11]
t.get([1,1,2])= { r: 11, i: 0 }
t=[[[0,1,2],[3,4,5]],[[6,7,8],[9,10,11]]]
t=[[0,1,2,3],[4,5,6,7],[8,9,10,11]]
nd=[[0,1,2,3],[4,5,6,7],[8,9,10,11]]
t2={"r":[0,1,2,3,4,5,6,7,8,9,10,11],"i":[],"shape":[3,4]}
t3={"r":[0,2,4,6,8,10,12,14,16,18,20,22],"i":[],"shape":[3,4]}
t4={"r":[0,0,0,0,0,0,0,0,0,0,0,0],"i":[],"shape":[3,4]}
```

