const E = module.exports = {}
const {factorial} = require('./function')
const {diffn} = require('./calculus')
let { pow } = Math

// https://en.wikipedia.org/wiki/Lagrange_polynomial
E.legendrePolynomial = function (x, n) {
  return 1/(pow(2, n)*factorial(n))*diffn((x)=>pow(x*x-1, n), n)
}
