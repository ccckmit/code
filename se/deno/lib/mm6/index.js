const uu6 = require('../../uu6')

const mm6 = module.exports = {
  R: require('./R'),
  P: require('./probability'),
  V: require('./vector'),
  G: require('./geometry'),
  T: require('./tensor'),
  M: require('./matrix'),
  S: require('./stat'),
  F: require('./function'),
  C: require('./constant'),
  // OO: require('./oo'),
  O: require('./O'),
  RA: require('./ratio')
  // D: require('./calculus'),
  // AD: require('../../nn6/src/autoDiff'),
  // argmax: require('./argmax'),
}

// mm6.oo = mm6.OO.oo
mm6.oo = mm6.O.oo

uu6.mixin(mm6, mm6.R, mm6.V, mm6.G, mm6.T, mm6.M, mm6.S, mm6.F, mm6.C, 
  require('./calculus'),
  // require('./point'),
  require('./complex'),
  require('./calculus'),
  require('./space'),
  require('./entropy'),
  require('./transform'),
  require('./optimize'),
  require('./series'),
  require('./ratio'),
  require('./ode'),
  Math,
)

