# deno doc

```
PS D:\ccc\ccc109a\se\deno\se\doc> deno doc add.ts       
function add(x: number, y: number): number
  Adds x and y.
  @param {number} x
  @param {number} y
  @returns {number} Sum of x and y
```