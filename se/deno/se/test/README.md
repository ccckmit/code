# deno test

* https://deno.land/manual/testing/assertions
* https://deno.land/std/testing/README.md
    * equal() - Deep comparison function, where actual and expected are compared deeply, and if they vary, equal returns false.

## test1.ts

```
PS D:\ccc\ccc109a\se\deno\_more\test> deno test test1.ts
Check file:///D:/ccc/ccc109a/se/deno/_more/test/.deno.test.ts
running 1 tests
test Test Assert ... ok (31ms)

test result: ok. 1 passed; 0 failed; 0 ignored; 0 measured; 0 filtered out (34ms)
```

## test2.ts

```
PS D:\ccc\ccc109a\se\deno\_more\test> deno test test2.ts
Check file:///D:/ccc/ccc109a/se/deno/_more/test/.deno.test.ts
running 1 tests
test example ... ok (13ms)

test result: ok. 1 passed; 0 failed; 0 ignored; 0 measured; 0 filtered out (155ms)
```
