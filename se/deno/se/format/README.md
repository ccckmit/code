# deno fmt

## 檢查

```
$ deno fmt --check test.js

from test.js:
1| -
2| -
3| -function add(a,b) {       
1| +function add(a, b) {      
4| -  return a             + b
5| -    }
2| +  return a + b;
3| +}

error: Found 1 not formatted file
```

## 直接格式化

```
$ deno fmt test.js
```
