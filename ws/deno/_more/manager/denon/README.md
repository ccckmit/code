# denon

* https://deno.land/x/denon@2.3.2

```
PS D:\ccc\ccc109a\ws\deno\_more\denon> deno install --allow-read --allow-run --allow-write --allow-net -f -q --unstable https://deno.land/x/denon@2.3.2/denon.ts
✅ Successfully installed denon

PS D:\ccc\ccc109a\ws\deno\_more\denon> denon --init json
[*] [main] v2.3.2-master
[*] [conf] writing template to `denon.json`
[*] [conf] `denon.json` created in current working directory

PS D:\ccc\ccc109a\ws\deno\_more\denon> denon start      
[*] [main] v2.3.2-master
[*] [daem] watching path(s): *.*
[*] [daem] watching extensions: ts,tsx,js,jsx,json
[!] [#0] starting main `deno run app.ts`
Hello 你好！
[*] [daem] clean exit - waiting for changes before restart
```