# webview

* https://github.com/webview/webview_deno
* https://denotutorials.net/making-desktop-gui-applications-using-deno-webview.html

```
user@DESKTOP-96FRN6B MINGW64 /d/ccc/ccc109a/ws/deno/_more/webview (master)
$ deno run -A -r --unstable hello.js
Download https://deno.land/x/webview/mod.ts
Compile https://deno.land/x/webview/mod.ts
Download https://deno.land/x/webview/plugin.ts
Download https://deno.land/x/webview/deps.ts
Download https://deno.land/x/plugin_prepare@v0.6.0/mod.ts
Download https://deno.land/std@0.56.0/async/mod.ts
Download https://deno.land/x/plugin_prepare@v0.6.0/deps.ts
Download https://deno.land/std@v0.50.0/fs/exists.ts
Download https://deno.land/x/checksum@1.4.0/mod.ts 
Download https://deno.land/x/checksum@1.4.0/hash.ts
Download https://deno.land/x/checksum@1.4.0/sha1.ts
Download https://deno.land/x/checksum@1.4.0/md5.ts
Download https://deno.land/std@0.56.0/async/deferred.ts
Download https://deno.land/std@0.56.0/async/delay.ts
Download https://deno.land/std@0.56.0/async/mux_async_iterator.ts
Download https://deno.land/std@v0.50.0/log/mod.ts
Download https://deno.land/std@v0.50.0/path/mod.ts
Compile https://deno.land/std@v0.50.0/log/mod.ts
Download https://deno.land/std@v0.50.0/log/logger.ts
Download https://deno.land/std@v0.50.0/log/handlers.ts
Download https://deno.land/std@v0.50.0/testing/asserts.ts
Download https://deno.land/std@v0.50.0/log/levels.ts
Download https://deno.land/std@v0.50.0/log/levels.ts
Download https://deno.land/std@v0.50.0/fmt/colors.ts
Download https://deno.land/std@v0.50.0/testing/diff.ts
Compile https://deno.land/std@v0.50.0/path/mod.ts
Download https://deno.land/std@v0.50.0/path/win32.ts
Download https://deno.land/std@v0.50.0/path/posix.ts
Download https://deno.land/std@v0.50.0/path/common.ts
Download https://deno.land/std@v0.50.0/path/separator.ts
Download https://deno.land/std@v0.50.0/path/interface.ts
Download https://deno.land/std@v0.50.0/path/glob.ts
Download https://deno.land/std@v0.50.0/path/_constants.ts
Download https://deno.land/std@v0.50.0/path/_util.ts
Download https://deno.land/std@v0.50.0/path/_globrex.ts
INFO downloading deno plugin "webview_deno" from "https://github.com/webview/webview_deno/releases/download/0.4.2/deno_webview.dll"
INFO load deno plugin "webview_deno" from local "d:\ccc\ccc109a\ws\deno\_more\webview\.deno_plugins\webview_deno_c6cdbebec70ea2806fd889ffdcfb65da.dll"
```

然後就出現了 Hello 網頁，Good !

