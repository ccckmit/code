import { WebView } from "https://deno.land/x/webview@0.4.5/mod.ts";
import { renderFileToString } from 'https://deno.land/x/dejs@0.8.0/mod.ts';

async function main() {
    let resp = await fetch("https://api.coindesk.com/v1/bpi/historical/close.json")
    let bpiData = await resp.json()
    // console.log('bpiData=', bpiData)

    // Rendering the template from the index.html file, including in
    // data for the Bitcoin Price Index when rendering the template
    let renderedTemplate = await renderFileToString("index.html", {
        bpiData: JSON.stringify(bpiData.bpi),
    });
    // console.log('renderedTemplate=', renderedTemplate)
    let html = "data:text/html," + renderedTemplate;
    
    // Creating the webview with the rendered template
    let webview = new WebView({
        title: "Deno Cryptocurrency Webview",
        url: html,
        width: 800,
        height: 600,
        resizable: true,
        debug: true,
        frameless: false
    });
    
    // Running the webview
    webview.run();
}

main()