# deno (more)

* [Deno进阶开发笔记 (不定时更新)](https://chenshenhai.com/deno_note)
    * [Rust 插件开发入门](https://chenshenhai.com/deno_note/note/chapter_08/deno_plugin_dev)
* [Deno 代码风格指南](https://deno.js.cn/t/topic/163)
* [Deno 中文手册](https://nugine.github.io/deno-manual-cn/)
    * https://denolang.cn/manual/
* [了不起的 Deno 实战教程](https://deno.js.cn/t/topic/35)
* [Deno 资源全图谱 · 专注简中版](https://github.com/hylerrix/awesome-deno-cn)
* https://deno.js.cn/
* https://denotutorials.net/
