

* [Jupyter Notebook: Embracing web standards (HTML/CSS/JS)](https://jupyter-notebook.readthedocs.io/en/stable/examples/Notebook/JavaScript%20Notebook%20Extensions.html)
* [Nodebooks: Introducing Node.js Data Science Notebooks](https://medium.com/codait/nodebooks-node-js-data-science-notebooks-aa140bea21ba)
    * import pixiedust_node