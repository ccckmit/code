const mount = require('koa-mount');
const Koa = require('koa');
const session = require('koa-session');

const CONFIG = {
  key: 'koa:sess', /** (string) cookie key (default is koa:sess) */
  /** (number || 'session') maxAge in ms (default is 1 days) */
  /** 'session' will result in a cookie that expires when session/browser is closed */
  /** Warning: If a session cookie is stolen, this cookie will never expire */
  maxAge: 86400000,
  autoCommit: true, /** (boolean) automatically commit headers (default true) */
  overwrite: true, /** (boolean) can overwrite or not (default true) */
  httpOnly: true, /** (boolean) httpOnly or not (default true) */
  signed: true, /** (boolean) signed or not (default true) */
  rolling: false, /** (boolean) Force a session identifier cookie to be set on every response. The expiration is reset to the original maxAge, resetting the expiration countdown. (default is false) */
  renew: false, /** (boolean) renew session when session is nearly expired, so we can always keep user logged in. (default is false)*/
};

// hello

const a = new Koa();

a.use(async function (ctx, next){
  await next();
  ctx.session.a = true
  console.log('a=', ctx.session.a, 'b=', ctx.session.b)
  ctx.body = 'Hello';
});

// world

const b = new Koa();

b.use(async function (ctx, next){
  ctx.session.b = true
  console.log('a=', ctx.session.a, 'b=', ctx.session.b)
  await next();
  ctx.body = 'World';
});

// app

const app = new Koa();
app.keys = ['some secret hurr'];
app.use(session(CONFIG, app));
app.use(mount('/hello', a));
app.use(mount('/world', b));

app.listen(3000);
console.log('listening on port 3000');