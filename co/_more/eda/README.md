# EDA

* [如何看待观点「华为没有核心技术，因为芯片用的是ARM（安谋）架构，一旦被停止授权，就会做不出芯片」？](https://www.zhihu.com/question/296178433)
* [中国人为什么不受待见？硅谷 eda 传奇，不用改编就是一部大片](https://wsdigest.com/article?artid=7070)
* [世界首個開源 PDK，Google 做到了](https://cdn.technews.tw/2020/07/13/open-source-pdk-google/)


對於任何人而言，如果想要製造晶片，在已經擁有 RTL（電阻晶體管邏輯電路）的前提下，還需要克服兩大障礙，一是從晶片代工廠獲得製程設計套件（PDK），二是有足夠的資金支付製造費用。

PDK 即 Process Design Kit 製程設計包，是連接 IC 設計公司、代工廠和 EDA 公司的橋梁。PDK 包括設計規則文件、電學規則文件、版圖層次定義文件、SPICE 仿真模型、器件版圖和期間定制參數。而 PDK 是將 RTL 轉化為物理晶片的關鍵步驟，也是精實流程開源的障礙，因此想製造自己的晶片，就要有一套可製造且開源的 PDK。

但在未來，製造自己的晶片將成為可能。最近，Google 的軟體工程師 Tim Ansell 公布了與 SkyWater 合作的第一個可製造的、開源的 PDK──SkyWater PDK。另外，無需承擔昂貴的製造費用，Google 也將提供完全免費的晶片製造流程。