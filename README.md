# 陳鍾誠的六門課程

課程                | 內容                  | 語言
-------------------|-----------------------|-------------
[網頁設計](wp/README.md)      | HTML / CSS / JavaScript / DOM / BOM  | [JavaScript]
[網站設計進階](ws/README.md)  | deno / oak / 資料庫 / SQL | [JavaScript]
[計算機結構](co/README.md)    | 數位邏輯 / nand2tetris  / HDL / CPU / 組合語言 | [Verilog]
[系統程式](sp/README.md)      | 編譯器 / 組譯器 / 虛擬機 / 作業系統 / gcc / msys2 / Linux | [C語言]
[軟體工程](se) 與 [演算法](alg) | git / 測試 / UML / 分析 / 設計 / 除錯 / 發佈 / 效能 / 品質  | [Rust]
[人工智慧](ai/README.md)      | AI / 神經網路 / 深度學習 / 科學計算 / 數學 / 物理 / 機率統計 | [Python]

> 作者: [陳鍾誠](http://www.nqu.edu.tw/educsie/index.php?act=blog&code=list&ids=4) 於 [金門大學](http://www.nqu.edu.tw/) [資訊工程系](http://www.nqu.edu.tw/educsie/index.php) 


[網頁設計]:wp/README.md
[網站設計進階]:ws/README.md
[計算機結構]:co/README.md
[系統程式]:sp/README.md
[軟體工程]:se/README.md
[演算法]:alg/
[資料庫]:db/
[人工智慧]:ai/README.md
[科學計算]:sci/
[數學]:math/
[物理]:physics/
[Python]:py/
[Rust]:rust/
[JavaScript]:js/
[C語言]:c/
[Verilog]:verilog/
[deno]:deno/
[Linux]:linux/
[gcc]:gcc/
[oak]:oak/
[PostgreSQL]:psql/
[git]:git/
[測試]:test/
[UML]:uml/
[分析]:analysis/
[設計]:design/
[效能]:performance/
